# Project MBKM
- Manufaktur
  1. Instalasi DCS
  2. Trackless AGV
  3. QC Camera
  4. Liquid handling
  5. Clip bumper
  6. Arm Transfer

- Alkes
  1. E-Nose
  2. Ventilator ICU
  3. Vakum Sedot Darah
  4. Medical Scale
  5. Ventilator R03
  6. Alat Cuci Darah

- Pendidikan
  1. MPS
  2. AR/VR untuk Medical Student
  3. Online Platform PAI

- Peternakan
  1. Digitalisasi Kandang Kambing
  2. Standarisasi Teknologi Pakan
  3. Standarisasi Proses Breeding, Fattening


# Kurikulum
Kurikulum MBKM terdiri dari 3 kategori :
1. Pengantar
2. Lanjutan
3. Praktik


## Pengantar
Di dalam pengantar, terdapat 3 kurikulum :

### nodejs  
poin penting yg disampaikan :
1. nodejs
2. modular
3. struktur direktori dan file di stechoq
4. module di npm

isi ppt :
1. Apa itu nodejs ? definisi & pengertian umum dari nodejs, npm, n dan pemrograman.
2. Mengapa kita memilih nodejs ? fleksibel & mudah dipelajari.
3. Dimana saja penerapan nodejs ? be, web fe, desktop & scripting.
4. Apa contoh penerapan nodejs di existed project ? dcs & ( yg scripting, semisal sadikun / medical scale ).
5. Adakah cuplikan contoh-contoh code nodejs ?  
  screenshoot / tampilkan penggunaan nodejs di be, web fe, desktop atau scripting,  
  untuk menunjukkan gambaran kasar mengenai nodejs.
6. Adakah module di npm yang perlu kita tau ?  
  contoh module yg kemungkinan dpt digunakan baik di be, web fe, desktop serta scripting,
  seperti eventbus, json5, lodash, date-and-time atau moment
7. Apakah kita harus selalu menggunakan module ?  
  jangan terlalu bergantung pada module, terutama apabila kita hanya membutuhkan sebagian dari module.  
  Intinya, sebutkan dampak negatif dari pecandu module.
8. Apakah bagian terpenting dlm pemrograman ketika mengerjakan project ? struktur file dan direktori serta prinsip modular.
7. Apa itu modular ? pengertian modular di pemrograman.
8. Mengapa kita perlu modular ? lbh mudah dibaca dan di-debug.
9. Adakah contoh penerapan modular di nodejs ?
  screenshoot / tampilkan struktur direktori dan struktur file (class, etc) di be, web fe, desktop atau scripting,  
  untuk menunjukkan gambaran kasar mengenai modular yg sedang dibahas.  

### linux (pake punyane mas iqro)
poin penting yg disampaikan :
1. instalasi linux, biar pada dual-boot linux atau mungkin ada fasilitas device yg sdh dipasang linux (kayak dcs atau nuc)
2. pengaturan jaringan & partisi di linux (nmtui, dll)
3. struktur direktori root (/home, /usr/local/bin, /root, etc.)
4. komunikasi dgn ssh (with password, authorized_key)
5. command yg sering dipake & basic shell scripting
6. clone & image c-fast

### database
poin penting yg disampaikan :
1. database, sql dan mysql
2. styling scheme mysql di stechoq

isi ppt :
1. Apa itu database ? definisi & pengertian umum dari database dan dbms.
2. SQL dan NoSQL ? pengertian, perbedaan dan contoh dari keduanya,
  semisal nosql : nyimpen data per baris di file (kayak log), redis maupun mongodb.
3. Database apa saja yg digunakan di stechoq ? mysql dan log file (nyimpen data per baris di file).
4. Kapan kita menggunakan log file ? di device atau output dari suatu program.
5. Apa dbms sql yg kita gunakan ? mysql, free.
6. Apa itu scheme dan normalisasi ?  
  Anggap peserta emang dari nol, dan kita perkenalkan materi dasar mengenai database, seperti : normalisasi dan acid,
  sebatas perkenalan aja, gk sampai mendalam.
7. Adakah styling scheme di stechoq ? 
  penamaan table : auth_, ref_, d_, s_,  
  dan penamaan kolom
8. Adakah cara untuk mengoptimalkan scheme mysql kita ?
    - scheme normalization, 
    - usahakan untuk menyimpan hasil kalkulasi ke db guna memperingan select yg complex, 
    - indexing, 
    - composite index
9. Apa saja syntax query mysql yg sering digunakan ?
    - crud (insert, select, update, delete)
    - join (left, right, cross, inner)
    - aggregation (group by, order by, limit, count, sum)
    - filter (where, having, if, ifnull)
10. Adakah cara untuk mengoptimalkan query mysql kita ?
    - usahakan untuk filter dulu baru join,
    - jangan terlalu banyak menggunakan union dalam satu query,
    - hilangkan penggunaan subquery diantara ```select``` dan ```from```,
    - referensi lain : http://mysql.rjweb.org/doc.php/ricksrots & relational algebra
11. Adakah feature lain yg sering digunakan di mysql ?  
  function, procedure, views, trigger dan virtual column,  
  beserta kapan penggunaannya.


## Lanjutan
Di dalam lanjutan terdapat 7 kurikulum :

### backend
poin penting yg disampaikan :
1. nodejs di backend
2. routing (expressjs) dan db (mariadb / mysql)
3. struktur direktori & file di stechoq
4. prosedur dokumentasi untuk routing, function dan general
5. deployment & logging (pm2)
6. unit testing

### frontend (vuejs, cypress / end-to-end testing)
poin penting yg disampaikan :
1. nodejs di fe, framework vuejs
2. penyimpanan (vuex, localStorage, session, cookies) & vue router
3. styling dg argon & bootstrap
4. struktur direktori & file di stechoq
5. prosedur dokumentasi untuk logika/kalkulasi, function dan general
6. cypress, end-to-end testing


### desktop (electron, qt)
poin penting yg disampaikan :
1. nodejs di desktop, electron
2. struktur direktori & file di stechoq
3. prosedur dokumentasi untuk logika/kalkulasi, function dan general
4. migrasi ke qt
5. unit testing ? (mungkin, next batch aja)

### shell scripting & git
poin penting yg disampaikan :
1. shell scripting, terutama device
2. struktur direktori & file di stechoq
3. service, crontab, autostart xdg
4. prosedur dokumentasi untuk logika/kalkulasi dan general
5. git & ci/cd
6. pastikan semua project ada git nya dan berada di bawah stechoq/

### Android (flutter & native)
poin penting yg disampaikan :
1. android native
2. flutter
3. prosedur dokumentasi untuk logika/kalkulasi dan general
4. migrasi program flutter ke android native (misal yg ar/vr)

### communication (serial, websocket, tcp, dkk)
poin penting yg disampaikan :
1. basic networking (iso layer, per protocol)
2. implementasi di pemrograman & linux
3. komponen paket data (address, response state, data, checksum) & wireshark

### (ai / machine learning / pattern recognition) & (AR / VR)
poin penting yg disampaikan :
1. ai untuk qc camera & e-nose
2. hololens & oculus
3. blender (3d desaign)
4. unity (mrogram 3d)

## Praktik
Praktik adalah segala sesuatu yg berhubungan dengan pengimplementasian materi yg sdh disampaikan.  
Praktik dapat dilakukan :  
- di sela2 penyampaian materi.
- ataupun di 2 minggu kedua.
